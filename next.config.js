module.exports = {
  webpack: function(config) {
    config.devtool = 'cheap-module-eval-source-map';
    config.module.rules.push({
      test: /\.md$/,
      use: 'raw-loader'
    })
    return config;
  },
  env: {
    recaptchaSiteKey: '6LezzvQUAAAAAH9cOSVQB3xOZErE7HFH3gwHyg3l'
  }
}