import React from 'react';
import HomePage from '../src/HomePage';
import Layout from '../src/Layout';

export default function Index() {

  return (
    <Layout>
      <HomePage page={1} isFirstPage={true}/>
    </Layout>  
  );
}