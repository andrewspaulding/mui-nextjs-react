import React from 'react';
import { useRouter } from 'next/router';
import HomePage from '../src/HomePage';
import Layout from '../src/Layout';

export default function Index() {

  const router = useRouter();
  const { page } = router.query;

  return (
    page ?
      <Layout>
        <HomePage page={page} isFirstPage={false} />
      </Layout>
      : null
  );
}