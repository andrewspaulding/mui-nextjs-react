import React from 'react';
import SignIn from '../src/SignIn';
import Layout from '../src/Layout';

export default function SignInPage() {

  return (
    <Layout>
      <SignIn isPage={true}/>
    </Layout>
  );
}