import React from 'react';
import SignUp from '../src/SignUp';
import Layout from '../src/Layout';

export default function SignUpPage() {

  return (
    <Layout>
      <SignUp isPage={true}/>
    </Layout>
  );
}