import React, { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import { fetchReadStory } from '../../../src/fetch/Story';
import parse from 'html-react-parser';
import Layout from '../../../src/Layout';

const useStyles = makeStyles(theme => ({
  paper: {
    margin: theme.spacing(8, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  }
}));

export default function ReadSlug() {
  const classes = useStyles();
  const router = useRouter();
  const { username, slug } = router.query;
  
  const [title, setTitle] = useState('');
  const [body, setBody] = useState('');

  useEffect(() => {
    fetchReadStory(username, slug)
      .then(data => {
        setTitle(data.title);
        setBody(parse(data.body));
      })
      .catch(error => {
        console.warn("Could not retrieve story:  " + error);
      });
  }, [username, slug]);

  return (
    <Layout>
      <Grid container justify="center" component="main" className={classes.root}>
        <CssBaseline />
        <Grid item xs={12} sm={8} md={5}>
          <div className={classes.paper}>
            <Typography component="h1" variant="h4">
              {title}
            </Typography>
          </div>
          <div className={classes.paper}>
            <Typography component="div" variant="body1">
              {body}
            </Typography>
          </div>
        </Grid>
      </Grid>
    </Layout>
  );
}