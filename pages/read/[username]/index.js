import React from 'react';
import { useRouter } from 'next/router';
import Layout from '../../../src/Layout';
import AuthorCards from '../../../src/AuthorCards';

export default function AuthorStories() {

  const router = useRouter();
  const { username } = router.query;

  return (
    <Layout>
      <AuthorCards username={username} pageName="Stories"/>
    </Layout>
  );
}