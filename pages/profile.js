import React, { useState } from 'react';
import { connect } from 'react-redux';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Hidden from '@material-ui/core/Hidden';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import { patchPassword } from '../src/fetch/UpdatePassword';
import { Divider } from '@material-ui/core';
import MessageDialog from '../src/MessageDialog';
import Layout from '../src/Layout';
import { updateProfile } from '../src/redux/modules/profile';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(8, 1, 1, 1)
  }
}));

const Profile = (props) => {

  const classes = useStyles();

  const [editName, setEditName] = useState('');
  const [editEmail, setEditEmail] = useState('');
  const [editDescription, setEditDescription] = useState('');
  const [editMode, setEditMode] = useState(false);
  const [editPasswordMode, setEditPasswordMode] = useState(false);
  const [currentPassword, setCurrentPassword] = useState('');
  const [newPassword, setNewPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [confirmMatchFailed, setConfirmMatchFailed] = useState(false);
  const [triggerError, setTriggerError] = useState(false);
  const [errorTitle, setErrorTitle] = useState('');
  const [errorMessage, setErrorMessage] = useState('');

  const handleCancel = () => {
    setEditMode(false);
  };

  const handleCancelPassword = () => {
    setEditPasswordMode(false);
    setCurrentPassword('');
    setNewPassword('');
    setConfirmPassword('');
    setConfirmMatchFailed(false);
  };

  const handleSave = () => {
    event.preventDefault();
    const payload = {
      name: editName,
      email: editEmail,
      aboutDescription: editDescription
    }
    props.updateProfile(payload);
    setEditMode(false);
  };

  const resetTrigger = () => {
    setTriggerError(false);
  };

  const handleSavePassword = () => {
    event.preventDefault();
    if(newPassword != confirmPassword) {
      setConfirmMatchFailed(true);
    }
    else {
      patchPassword(currentPassword, newPassword)
        .then(data => {
          if (data.success === true) {
            console.log("Password update successful");
          }
          setErrorTitle("Success");
          setErrorMessage("Password update successful");
          setTriggerError(true);
          setEditPasswordMode(false);
        })
        .catch(error => {
          console.warn("Could not save password:  " + error);
          setErrorTitle("Password update failed");
          setErrorMessage(error.message);
          setTriggerError(true);
        })
    }
  };

  const handleEditProfile = () => {
    setEditName(props.name);
    setEditEmail(props.email);
    setEditDescription(props.description);
    setEditMode(true);
  };

  const handleEditPassword = () => {
    setEditPasswordMode(true);
  };

  return (
    <Layout isSecurePage={true}>
      <form className={classes.form} onSubmit={handleSave.bind(this)}>
        <Grid container spacing={5} className={classes.root}>
          <Grid container item justify="center">
            <Grid container item xs={10} sm={8} md={5}>
              <Grid item xs={12}>
                <Typography component="h1" variant="h4" gutterBottom>
                  @{props.username}
                </Typography>
              </Grid>
              <Grid item xs={12}>
                <Typography component="h1" variant="h4" gutterBottom>
                  Profile Info
                </Typography>
              </Grid>
            </Grid>
          </Grid>
          <Grid container item justify="center">
            <Grid container item xs={10} sm={8} md={5} alignItems="center">
              <Hidden xsDown={editMode ? true : false}>
                <Grid item xs={12} sm={4} md={4} lg={3}>
                  <Typography component="span" variant="h6">
                    Name:
                  </Typography>
                </Grid>
              </Hidden>
              {!editMode &&
                <Grid item xs={12} sm={8} md={8} lg={9}>
                  <Typography component="span" variant="h6" gutterBottom>
                    {props.name}
                  </Typography>
                </Grid>
              }
              {editMode &&
                <Grid item xs={12} sm={8} md={8} lg={9}>
                  <TextField
                    variant="outlined"
                    value={editName}
                    onChange={(event) => setEditName(event.target.value)}
                    margin="none"
                    required
                    fullWidth
                    name="name"
                    label="Name"
                    type="name"
                    id="name"
                  />
                </Grid>
              }
            </Grid>
          </Grid>
          <Grid container item justify="center">
            <Grid container item xs={10} sm={8} md={5} alignItems="center">
              <Hidden xsDown={editMode ? true : false}>
                <Grid item xs={12} sm={4} md={4} lg={3}>
                  <Typography component="span" variant="h6">
                    Email:
              </Typography>
                </Grid>
              </Hidden>
              {!editMode &&
                <Grid item xs={12} sm={8} md={8} lg={9}>
                  <Typography component="span" variant="h6" gutterBottom>
                    {props.email}
                  </Typography>
                </Grid>
              }
              {editMode &&
                <Grid item xs={12} sm={8} md={8} lg={9}>
                  <TextField
                    variant="outlined"
                    value={editEmail}
                    onChange={(event) => setEditEmail(event.target.value)}
                    margin="none"
                    required
                    fullWidth
                    name="email"
                    label="Email"
                    type="email"
                    id="email"
                  />
                </Grid>
              }
            </Grid>
          </Grid>
          <Grid container item justify="center">
            <Grid container item xs={10} sm={8} md={5} alignItems="center">
              <Hidden xsDown={editMode ? true : false}>
                <Grid item xs={12} sm={4} md={4} lg={3}>
                  <Typography component="span" variant="h6">
                    Description:
              </Typography>
                </Grid>
              </Hidden>
              {!editMode &&
                <Grid item xs={12} sm={8} md={8} lg={9}>
                  <Typography component="span" variant="h6" gutterBottom>
                    {props.description}
                  </Typography>
                </Grid>
              }
              {editMode &&
                <Grid item xs={12} sm={8} md={8} lg={9}>
                  <TextField
                    variant="outlined"
                    value={editDescription}
                    onChange={(event) => setEditDescription(event.target.value)}
                    margin="none"
                    fullWidth
                    name="description"
                    label="Description"
                    type="description"
                    id="description"
                  />
                </Grid>
              }
            </Grid>
          </Grid>
          {!editMode &&
            <Grid container item justify="center">
              <Grid container item xs={10} sm={8} md={5} alignItems="center">
                <Grid item xs={12} sm={4} md={4} lg={3}>
                  <Typography component="span" variant="h6">
                    Created Date:
              </Typography>
                </Grid>
                <Grid item xs={12} sm={8} md={8} lg={9}>
                  <Typography component="span" variant="h6" gutterBottom>
                    {props.createdDate}
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
          }
          {!editMode &&
            <Grid container item justify="center">
              <Grid item xs={10} sm={8} md={5}>
                <Button variant="outlined" color="primary" onClick={handleEditProfile}>
                  Edit Profile
            </Button>
              </Grid>
            </Grid>
          }
          {editMode &&
            <Grid container item justify="center">
              <Grid item xs={5} sm={4} md={2}>
                <Button variant="outlined" color="secondary" onClick={handleCancel}>
                  Cancel
            </Button>
              </Grid>
              <Grid item xs={5} sm={4} md={2}>
                <Button variant="outlined" color="primary" type="submit">
                  Save
            </Button>
              </Grid>
            </Grid>
          }
        </Grid>
      </form>
      <Box my={5}>
        <Grid container justify="center">
          <Grid item xs={11} sm={9} md={6}>
            <Divider></Divider>
          </Grid>
        </Grid>
      </Box>
      <form className={classes.form} onSubmit={handleSavePassword.bind(this)}>
        <Grid container spacing={5}>
          <Grid container item justify="center">
            <Grid container item xs={10} sm={8} md={5}>
              <Grid item xs={12}>
                <Typography component="h1" variant="h4" gutterBottom>
                  Password
                </Typography>
              </Grid>
            </Grid>
          </Grid>
          {!editPasswordMode &&
            <Grid container item justify="center">
              <Grid item xs={10} sm={8} md={5}>
                <Button variant="outlined" color="primary" onClick={handleEditPassword}>
                  Edit Password
            </Button>
              </Grid>
            </Grid>
          }
          {editPasswordMode &&
            <React.Fragment>
              <Grid container item justify="center">
                <Grid container item xs={10} sm={8} md={5} alignItems="center">
                  <Hidden xsDown={editPasswordMode ? true : false}>
                    <Grid item xs={4} sm={4} md={4} lg={3}>
                      <Typography component="span" variant="h6">
                        Current Password:
                      </Typography>
                    </Grid>
                  </Hidden>
                  <Grid item xs={12} sm={8} md={8} lg={9}>
                    <TextField
                      variant="outlined"
                      value={currentPassword}
                      onChange={(event) => setCurrentPassword(event.target.value)}
                      margin="none"
                      required
                      fullWidth
                      name="current-password"
                      label="Current Password"
                      type="password"
                      autoComplete="current-password"
                      id="current-password"
                    />
                  </Grid>
                </Grid>
              </Grid>
              <Grid container item justify="center">
                <Grid container item xs={10} sm={8} md={5} alignItems="center">
                  <Hidden xsDown={editPasswordMode ? true : false}>
                    <Grid item xs={4} sm={4} md={4} lg={3}>
                      <Typography component="span" variant="h6">
                        New Password:
                    </Typography>
                    </Grid>
                  </Hidden>
                  <Grid item xs={12} sm={8} md={8} lg={9}>
                    <TextField
                      variant="outlined"
                      value={newPassword}
                      onChange={(event) => setNewPassword(event.target.value)}
                      margin="none"
                      required
                      fullWidth
                      name="new-password"
                      label="New Password"
                      type="password"
                      id="new-password"
                    />
                  </Grid>
                </Grid>
              </Grid>
              <Grid container item justify="center">
                <Grid container item xs={10} sm={8} md={5} alignItems="center">
                  <Hidden xsDown={editPasswordMode ? true : false}>
                    <Grid item xs={4} sm={4} md={4} lg={3}>
                      <Typography component="span" variant="h6">
                        Confirm Password:
                  </Typography>
                    </Grid>
                  </Hidden>
                  <Grid item xs={12} sm={8} md={8} lg={9}>
                    <TextField
                      variant="outlined"
                      value={confirmPassword}
                      onChange={(event) => setConfirmPassword(event.target.value)}
                      margin="none"
                      required
                      fullWidth
                      name="confirm-password"
                      label="Confirm Password"
                      type="password"
                      id="confirm-password"
                      error={confirmMatchFailed}
                      helperText={confirmMatchFailed ? "Passwords must match" : ""}
                      onFocus={() => setConfirmMatchFailed(false)}
                    />
                  </Grid>
                </Grid>
              </Grid>
              <Grid container item justify="center">
                <Grid item xs={5} sm={4} md={2}>
                  <Button variant="outlined" color="secondary" onClick={handleCancelPassword}>
                    Cancel
                  </Button>
                </Grid>
                <Grid item xs={5} sm={4} md={2}>
                  <Button variant="outlined" color="primary" type="submit">
                    Save
                  </Button>
                </Grid>
              </Grid>
            </React.Fragment>
          }
        </Grid>
      </form>
      <MessageDialog
        title={errorTitle}
        message={errorMessage}
        resetTrigger={resetTrigger}
        triggered={triggerError}>
      </MessageDialog>
    </Layout>
  );
};

const mapStateToProps = state => {
  const { profile } = state
  const { details } = profile

  return (details != null)
    ? {
      username: details.username,
      name: details.name,
      email: details.email,
      createdDate: details.createdDate,
      description: details.aboutDescription
    }
    : {
      profileError: state.profileError
    }
};

const mapDispatchToProps = { updateProfile };

export default connect(mapStateToProps, mapDispatchToProps)(Profile);