import React from 'react';
import ForgotPassword from '../src/ForgotPassword';
import Layout from '../src/Layout';

export default function ForgotPasswordPage() {

  return (
    <Layout>
      <ForgotPassword isPage={true}/>
    </Layout>
  );
}