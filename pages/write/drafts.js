import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import { fetchProfileDrafts } from '../../src/fetch/Writing';
import Layout from '../../src/Layout';
import CssBaseline from '@material-ui/core/CssBaseline';
import DraftCard from '../../src/DraftCard';

const useStyles = makeStyles(theme => ({
  root: {
    //height: '100vh',
  },
  paper: {
    margin: theme.spacing(8, 1, 1, 1),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
}));

const Drafts = (props) => {

  const classes = useStyles();

  const [drafts, setDrafts] = useState();

  useEffect(() => {
    if(props.username) {
      fetchProfileDrafts()
        .then(data => {
          if(data) {
            const mappedData = data.map((draft, index) => {
              if(draft.lastModifiedDate) {
                return (
                  <Grid key={index} item xs={12} sm={6} md={4}>
                    <DraftCard
                      title={draft.title}
                      id={draft.id}
                      snippet={draft.snippet}
                      lastModifiedDate={draft.lastModifiedDate}
                      lastPublishedDate={draft.lastPublishedDate}
                    />
                  </Grid>
                )
              }
            });
            setDrafts(mappedData);
          }
        })
        .catch(error => {
          console.warn("Could not retrieve author:  " + error);
        });
    }
  }, [props.username]);

  return (
    <Layout isSecurePage={true}>
      <Grid container justify="center" component="main" className={classes.root}>
        <CssBaseline />
        <Grid item xs={12} sm={10} lg={8}>
          <div className={classes.paper}>
            <Box mb={5}>
              <Typography component="h1" variant="h5">
                {'@' + props.username + ' Drafts'}
              </Typography>
            </Box>
            <Grid container>
              {drafts}
            </Grid>
          </div>
        </Grid>
      </Grid>
    </Layout>
  );
};

const mapStateToProps = state => {
  const { profile } = state
  const { details } = profile

  return (details != null)
    ? {
      username: details.username
    }
    : {
      profileError: state.profileError
    }
};

export default connect(mapStateToProps)(Drafts);