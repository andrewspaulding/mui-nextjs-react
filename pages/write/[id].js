import React from 'react';
import dynamic from 'next/dynamic';
import { useRouter } from 'next/router';
import Layout from '../../src/Layout';

function showLoading() {
  return <p>Loading ...</p>;
}

showLoading.displayName = "ShowLoading";

const QuillNoSSRWrapper = dynamic(import('../../src/QuillEditor'), {
  ssr: false,
  loading: showLoading
});


export default function Write() {
  const router = useRouter();
  const { id } = router.query;

  return (
    <Layout>
      <QuillNoSSRWrapper id={id}/>
    </Layout>
  );
}