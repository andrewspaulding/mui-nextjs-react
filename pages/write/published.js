import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import { fetchProfilePublished } from '../../src/fetch/Writing';
import Layout from '../../src/Layout';
import CssBaseline from '@material-ui/core/CssBaseline';
import PublishedCard from '../../src/PublishedCard';

const useStyles = makeStyles(theme => ({
  root: {
    //height: '100vh',
  },
  paper: {
    margin: theme.spacing(8, 1, 1, 1),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
}));

const Published = (props) => {

  const classes = useStyles();

  const [writings, setWritings] = useState();

  useEffect(() => {
    if(props.username) {
      fetchProfilePublished()
        .then(data => {
          if(data) {
            const mappedData = data.map((writing, index) =>
              <Grid key={index} item xs={12} sm={6} md={4}>
                <PublishedCard
                  title={writing.title}
                  route={writing.publishedRoute}
                  snippet={writing.snippet}
                  lastModifiedDate={writing.lastModifiedDate}
                  publicationDate={writing.lastPublishedDate}
                />
              </Grid>
            );
            setWritings(mappedData);
          }
        })
        .catch(error => {
          console.warn("Could not retrieve author:  " + error);
        });
    }
  }, [props.username]);

  return (
    <Layout isSecurePage={true}>
      <Grid container justify="center" component="main" className={classes.root}>
        <CssBaseline />
        <Grid item xs={12} sm={10} lg={8}>
          <div className={classes.paper}>
            <Box mb={5}>
              <Typography component="h1" variant="h5">
                {props.username ? '@' + props.username + ' Published' : null}
              </Typography>
            </Box>
            <Grid container>
              {writings}
            </Grid>
          </div>
        </Grid>
      </Grid>
    </Layout>
  );
};

const mapStateToProps = state => {
  const { profile } = state
  const { details } = profile

  return (details != null)
    ? {
      username: details.username
    }
    : {
      profileError: state.profileError
    }
};

export default connect(mapStateToProps)(Published);