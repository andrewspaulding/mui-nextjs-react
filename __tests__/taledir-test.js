const puppeteer = require('puppeteer');

(async () => {
  const browser = await puppeteer.launch({
    ignoreHTTPSErrors: true
  });

  const url = 'https://taledir-test.com';

  const page = await browser.newPage();
  await page.goto(url);
  await page.screenshot({ path: 'taledir-test.png' });

  await capture(browser, url);

  await browser.close();
})();

const wait = (ms) => {
  return new Promise(resolve => setTimeout(() => resolve(), ms));
}

const capture = async (browser, url) => {
  // Load the specified page
  const page = await browser.newPage();
  await page.goto(url, {waitUntil: 'load'});

  // Get the height of the rendered page
  const bodyHandle = await page.$('body');
  const { height } = await bodyHandle.boundingBox();
  await bodyHandle.dispose();

  // Scroll one viewport at a time, pausing to let content load
  const viewportHeight = page.viewport().height;
  let viewportIncr = 0;
  while (viewportIncr + viewportHeight < height) {
    console.log("hi");
    await page.evaluate(_viewportHeight => {
      window.scrollBy(0, _viewportHeight);
    }, viewportHeight);
    await wait(200);
    viewportIncr = viewportIncr + viewportHeight;
  }

  // Scroll back to top
  // await page.evaluate(_ => {
  //   window.scrollTo(0, 0);
  // });

  // Some extra delay to let images load
  await wait(1000);

  return await page.screenshot({path: 'taledir-test2.png'});
}