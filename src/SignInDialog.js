import React from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import SignIn from './SignIn';
import SignUp from './SignUp';
import ForgotPassword from './ForgotPassword';
import { closeLogin } from './redux/modules/auth';

const styles = (theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
});

const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Box pr={6}>
        <Typography variant="h6">{children}</Typography>
      </Box>
      {onClose ? (
        <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const SignInDialog = props => {

  const handleClose = () => {
    props.closeLogin();
  };

  let displayForm;

  if(props.authDisplayForm === "Sign Up") {
    displayForm = <SignUp isPage={false}/>;
  }
  else if(props.authDisplayForm === "Forgot Password"){
    displayForm = <ForgotPassword isPage={false}/>;
  }
  else {
    displayForm = <SignIn isPage={false}/>;
  }  

  return (
    <div>
      <Dialog onClose={handleClose} aria-labelledby="customized-dialog-title" open={props.displayAuth}>
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
          {props.authDisplayForm}
        </DialogTitle>
        <DialogContent dividers>
          {displayForm}
        </DialogContent>
      </Dialog>
    </div>
  );
}

const mapStateToProps = state => {
  const { auth } = state
  return {
    displayAuth: auth.displayAuth,
    authDisplayForm: auth.authDisplayForm
  }
};

const mapDispatchToProps = { closeLogin };

export default connect(mapStateToProps, mapDispatchToProps)(SignInDialog);