import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  toolbar : {
    border: 'none !important',

    '& .ql-stroke' : {
      fill: 'none',
      stroke: '#ccc'
    },
  
    '& .ql-fill' : {
      fill: '#ccc',
      stroke: 'none'
    },
  
    '& .ql-picker-label' : {
      color: "#ccc"
    }
  }
}));

export default function QuillToolbar() {
  const classes = useStyles();

  return (
  <div id="toolbar" className={classes.toolbar}>
    <span className="ql-formats">
      <select className="ql-size" defaultValue={""} onChange={e => e.persist()}>
        <option value="small"></option>
        <option defaultValue></option>
        <option value="large"></option>
        <option value="huge"></option>
      </select>
    </span>
    <span className="ql-formats">
      <button className="ql-bold"/>
      <button className="ql-italic"></button>
      <button className="ql-underline"></button>
      <button className="ql-strike"></button>
    </span>
    <span className="ql-formats">
      <button className="ql-list" value="bullet"/>
      <button className="ql-list" value="ordered"/>
    </span>
    <span className="ql-formats">
      <button className="ql-link"></button>
    </span>
    <span className="ql-formats">
      <button className="ql-clean"></button>
    </span>
  </div>
  );
}