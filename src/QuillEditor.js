import React, { useState, useEffect, useRef } from 'react';
import { fetchWriting, postSaveWriting, postSaveTitle } from './fetch/Writing';
import Delta from 'quill-delta';
// import ReactQuill from './ReactQuill';
import ReactQuill from 'react-quill';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import QuillToolbar from './QuillToolbar';

const useStyles = makeStyles(theme => ({
  paper: {
    margin: theme.spacing(2, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },

  editor: {
    [theme.breakpoints.up('sm')]: {
      // border: '1px solid #ccc',
      height: 'calc(90vh - 90px)'
    },

    [theme.breakpoints.down('sm')]: {
      // border: '1px solid #ccc',
      // height: 'calc(90vh - 90px)'
      height: 'auto'
    },

    '& .ql-container' : {
      border: 'none'
    },

    '& .ql-editor' : {
      fontSize: 'initial',
      fontFamily: theme.typography.fontFamily
    },

    '& .ql-editor.ql-blank::before' : {
      color: 'inherit'
    },

    '& .ql-editor::-webkit-scrollbar' : {
      width: '1em'
    },
     
    '& .ql-editor::-webkit-scrollbar-track' : {
      boxShadow: 'inset 0 0 6px rgba(0, 0, 0, 0.3)'
    },
     
    '& .ql-editor::-webkit-scrollbar-thumb' : {
      backgroundColor: 'transparent',
      // outline: '1px solid slategrey'
    }
  }

}));

const formats = [
  'header',
  'font',
  'size',
  'bold',
  'italic',
  'underline',
  'strike',
  'blockquote',
  'list',
  'bullet',
  'indent',
  'link',
];

const customToolbar = <QuillToolbar/>;

const modules = {
  toolbar: {
    container: "#toolbar"
  },
  clipboard: {
    // toggle to add extra line breaks when pasting HTML:
    matchVisual: false,
  },
};  

const QuillEditor = (props) => {
  const classes = useStyles();

  const [firstLoad, setFirstLoad] = useState(true);
  const [saveInProgress, setSaveInProgress] = useState(false);
  const [waitingChanges, setWaitingChanges] = useState(new Delta());
  const [text, setText] = useState();
  const [change, setChange] = useState(new Delta());

  const [title, setTitle] = useState('');
  const [editTitle, setEditTitle] = useState(false);

  const initText = useRef(text);
  const initTitle = useRef(title);

  useEffect(() => {
    let tempText = initText.current;
    let tempTitle = initTitle.current;
    fetchWriting(props.id)
      .then(data => {
        tempText = new Delta(data.body);
        tempTitle = data.title;
      })
      .catch(error => {
        console.warn("Could not retrieve story:  " + error);
      })
      .finally(() => {
        setText(tempText);
        setTitle(tempTitle);
      })
  }, [props.id]);

  useEffect(() => {
    if(!saveInProgress && waitingChanges.length() > 0) {
      const mergedDelta = change.compose(waitingChanges)
      setChange(mergedDelta);
      setWaitingChanges(new Delta());
    }
  }, [saveInProgress, waitingChanges, change]);

  useEffect(() => {
    const savePending = setTimeout(() => {
      if (change.length() > 0) {
       setSaveInProgress(true);
        let tempChange;
        postSaveWriting(props.id, change)
          .then(() => {
            tempChange = new Delta();
          })
          .catch(error => {
            console.error("Autosave Failed:  " + error);
            tempChange = change;
          })
          .finally(() => {
            setChange(tempChange);
            setSaveInProgress(false);
          })
      }
    }, 500);

    return () => clearTimeout(savePending);

  }, [change, props.id]);

  const handleChange = (content, delta) => {

    setText(content);

    if (firstLoad) {
      setFirstLoad(false);
      setWaitingChanges(new Delta());
      setChange(new Delta());
    }
    else if (saveInProgress) {
      const newChange = waitingChanges.compose(delta)
      setWaitingChanges(newChange);
    }
    else {
      const newChange = change.compose(delta)
      setChange(newChange);
    }
  }

  const handleSaveTitle = () => {
    if(title) {
      postSaveTitle(props.id, title)
        .then(() => {
          setEditTitle(false);
        })
        .catch(error => {
          console.error("Save title failed:  " + error);
        })
    }
    else {
      setEditTitle(false);
    }
  };

  return (
    <Grid container justify="center" component="main" className={classes.root}>
      {text != null ?
        <Grid item xs={12} sm={10} md={8} lg={6}>
          <div className={classes.paper}>
            {editTitle ?
              <TextField
                variant="outlined"
                margin="none"
                required
                fullWidth
                id="title"
                label="Title"
                name="title"
                value={title || ''}
                autoFocus={true}
                onChange={(event) => setTitle(event.target.value)}
                onBlur={handleSaveTitle}
              /> :
              <Box mb={2}>
                <Typography component="h1" variant="h4" onClick={() => setEditTitle(true)}>
                  {title || "Click here to add title"}
                </Typography>
              </Box>
            }
          </div>
          <div>
            {customToolbar}
            <ReactQuill theme="snow"
              modules={modules}
              formats={formats}
              placeholder="Start writing here..."
              value={text}
              onChange={handleChange.bind(this)}
              className={classes.editor}
            />
          </div>
        </Grid> :
        null
      }
    </Grid>
  );
}

export default QuillEditor;