import React, { useState, useEffect, useRef } from 'react';
import Router from 'next/router';
import Link from './Link';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import MuiLink from '@material-ui/core/Link';
import CssBaseline from '@material-ui/core/CssBaseline';
import StoryCard from '../src/StoryCard';
import { fetchStoryCards } from '../src/fetch/Story';
import 'intersection-observer'; // polyfill
import Observer from '@researchgate/react-intersection-observer';

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <MuiLink color="inherit" href="https://material-ui.com/">
        Your Website
      </MuiLink>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const useStyles = makeStyles(theme => ({
  root: {
    //height: '100vh',
  },
  paper: {
    margin: theme.spacing(8, 1, 1, 1),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
}));

const HomePage = props => {

  const limit = 12;

  const classes = useStyles();
  const [stories, setStories] = useState([]);
  const [isMore, setIsMore] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const offset = useRef(props.page - 1);

  const updateUrl = () => {
    const pageNumber = offset.current + 1;
    const asUrl = '/' + pageNumber;
    const pageUrl = props.isFirstPage ? "/" : "/[page]";
    Router.push(pageUrl, asUrl, { shallow: true });
  };

  const incrementOffset = () => {
    offset.current = offset.current + 1;
    setIsLoading(true);
    fetchStoryCards(offset.current, limit)
      .then(data => {
        if(data) {
          if(data.results && data.results.length > 0) {
            updateUrl();
            const startIndex = stories.length;
            const mappedData = data.results.map((story, index) =>
              <Grid key={index + startIndex} item xs={12} sm={6} md={4}>
                <StoryCard
                  title={story.title}
                  route={story.route}
                  snippet={story.snippet}
                  authors={story.authors}
                />
              </Grid>
            );
            setStories([...stories, ...mappedData]);
            if(data.results.length < limit) {
              setIsMore(false);
            }
          }
          else {
            setIsMore(false);
          }
        }
      })
      .catch(error => {
        console.warn("Could not retrieve author:  " + error);
      })
      .finally(() => {
        setIsLoading(false);
      });
  }

  // initial load remains the same
  useEffect(() => {
    fetchStoryCards(offset.current, limit)
      .then(data => {
        if(data) {
          if(data.results && data.results.length > 0) {
            const mappedData = data.results.map((story, index) =>
              <Grid key={index} item xs={12} sm={6} md={4}>
                <StoryCard
                  title={story.title}
                  route={story.route}
                  snippet={story.snippet}
                  authors={story.authors}
                />
              </Grid>
            );
            setStories(mappedData);
            if(data.results.length >= limit) {
              setIsMore(true);
            }
          }
        }
      })
      .catch(error => {
        console.warn("Could not retrieve author:  " + error);
      });
  }, []);

  const handleIntersection = (event) => {
    if(event.isIntersecting) {
      incrementOffset();
    }
  }

  const handleShowMore = (event) => {
    event.preventDefault();
    incrementOffset();
  }

  return (
    <Grid container justify="center" component="main" className={classes.root}>
      <CssBaseline />
      <Grid item xs={12} sm={10} lg={8}>
        <div className={classes.paper}>
          <Box mb={5}>
            <Typography component="h1" variant="h5">
              Check out these great stories!
            </Typography>
          </Box>
          <Grid container>
            {stories}
          </Grid>
          {isMore
            ? <Observer 
                onChange={handleIntersection} 
                disabled={isLoading}
                rootMargin="0px 0px 300px">
                <Box mt={5}>
                  <Link href="#" onClick={handleShowMore} variant="body2">
                    Show More
                  </Link>
                </Box>
              </Observer>
            : null
          }
          <Box mt={5}>
            <Copyright />
          </Box>
        </div>
      </Grid>
    </Grid>
  );
}

export default HomePage;