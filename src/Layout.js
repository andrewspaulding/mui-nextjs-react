import React, { useEffect, useCallback } from 'react';
import { connect } from 'react-redux';
import Router, { withRouter } from 'next/router'
import { postRefresh, redirectToSignIn } from './redux/modules/auth';
import Cookies from 'js-cookie';
import Container from '@material-ui/core/Container';
import Header from './Header';
import SignInDialog from './SignInDialog';

const Layout = ({router, ...props}) => {

  const postRefresh = useCallback(props.postRefresh, [])
  const redirectToSignIn = useCallback(props.redirectToSignIn, [])
  const pathname = router.asPath

  let refreshNeeded = false;

  if(!props.isAuthState) {
    const isAuthCookie = Cookies.get('isAuthenticated');
    if(isAuthCookie) {
      refreshNeeded = true;
    }
  }

  useEffect(() => {
    if(refreshNeeded) {
      postRefresh();
    }
  }, [postRefresh, refreshNeeded]);

  useEffect(() => {
    if(props.isSecurePage && !props.isAuthState && !refreshNeeded) {
      redirectToSignIn(pathname);
      Router.push('/sign-in');
    }
  });

  return (
    <Container maxWidth={false} disableGutters={true}>
      <Header 
        pathname={pathname} 
        redirectToSignIn={redirectToSignIn}
        isSecurePage={props.isSecurePage}/>
      {props.children}
      <SignInDialog/>
    </Container>
  );
}

const mapStateToProps = state => {
  const { auth } = state
  return {
    isAuthState: auth.isAuthenticated,
    displayAuth: auth.displayAuth
  }
};

const mapDispatchToProps = { postRefresh, redirectToSignIn };

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Layout));