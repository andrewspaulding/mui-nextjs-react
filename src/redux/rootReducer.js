import { combineReducers } from '@reduxjs/toolkit'

import authReducer from './modules/auth'
import profileReducer from './modules/profile'

const rootReducer = combineReducers({
  auth: authReducer,
  profile: profileReducer
})

export default rootReducer