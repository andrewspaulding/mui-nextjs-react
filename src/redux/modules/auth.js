import { createSlice } from '@reduxjs/toolkit'
import Router from 'next/router'
import { 
  postRefreshToken, 
  postLogout,
  postPasswordResetEmail,
  postResetPassword } from '../../fetch/Auth'
import { getProfile, clearSessionProfile } from './profile'

const refreshInterval = 60000

const initialState = {
  isInitiated: false,
  isAuthenticated: false,
  authLoading: false,
  authError: null,
  displayAuth: false,
  isRefreshCycleActive: false,
  authDisplayForm: "Sign In",
  authSuccessRedirectPage: "/",
  displayPasswordResetTokenForm: false
}

const authStart = (state) => {
  state.authLoading = true
  state.authError = null
}

const authSuccess = (state, isAuthenticated) => {
  state.authError = null
  state.displayAuth = false
  state.authDisplayForm = "Sign In"
  state.isAuthenticated = isAuthenticated
  state.authLoading = false
  state.isInitiated = true
}

const authFailure = (state, action) => {
  state.authError = action.payload
  state.isAuthenticated = false
  state.authLoading = false
  state.isInitiated = true
}

const auth = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    refreshStart(state) {
      authStart(state)
    },
    refreshSuccess(state) {
      authSuccess(state, true)
    },
    refreshFailure(state, action) {
      authFailure(state, action)
      state.displayAuth = true
    },
    loginStart(state) {
      authStart(state)
    },
    loginSuccess(state) {
      authSuccess(state, true)
    },
    loginFailure(state, action) {
      authFailure(state, action)
    },
    logoutStart(state) {
      authStart(state)
    },
    logoutSuccess(state) {
      authSuccess(state, false)
    },
    logoutFailure(state, action) {
      state.authError = action.payload
      state.authLoading = false
    },
    emailPasswordResetStart(state) {
      authStart(state)
      state.displayPasswordResetTokenForm = false
    },
    emailPasswordResetSuccess(state) {
      state.authError = null
      state.authLoading = false
      state.displayPasswordResetTokenForm = true
    },
    emailPasswordResetFailure(state, action) {
      state.authError = action.payload
      state.authLoading = false
    },
    resetPasswordStart(state) {
      authStart(state)
    },
    resetPasswordSuccess(state) {
      state.authError = null
      state.authDisplayForm = "Sign In"
      state.authLoading = false
      state.displayPasswordResetTokenForm = false
    },
    resetPasswordFailure(state, action) {
      state.authError = action.payload
      state.authLoading = false
    },
    startRefreshCyle(state) {
      state.isRefreshCycleActive = true
    },
    stopRefreshCyle(state) {
      state.isRefreshCycleActive = false
    },
    openLogin(state) {
      state.displayAuth = true
    },
    closeLogin(state) {
      state.displayAuth = false
      state.authDisplayForm = "Sign In"
    },
    showSignUp(state) {
      state.authDisplayForm = "Sign Up"
    },
    showSignIn(state) {
      state.authDisplayForm = "Sign In"
    },
    showForgotPassword(state) {
      state.displayPasswordResetTokenForm = false
      state.authDisplayForm = "Forgot Password"
    },
    redirectToSignIn(state, action) {
      state.authSuccessRedirectPage = action.payload
    },
    clearError(state) {
      state.authError = null
    }
  }
})

export const {
  refreshStart,
  refreshSuccess,
  refreshFailure,
  loginStart,
  loginSuccess,
  loginFailure,
  logoutStart,
  logoutSuccess,
  logoutFailure,
  emailPasswordResetStart,
  emailPasswordResetSuccess,
  emailPasswordResetFailure,
  resetPasswordStart,
  resetPasswordSuccess,
  resetPasswordFailure,
  startRefreshCyle,
  stopRefreshCyle,
  openLogin,
  closeLogin,
  showSignUp,
  showSignIn,
  showForgotPassword,
  redirectToSignIn,
  clearError
} = auth.actions
export default auth.reducer

export const handleLoginSuccess = () => async (dispatch, getState) => {
  const isAuthBefore = getState().auth.isAuthenticated
  dispatch(loginSuccess())
  const isAuthAfter = getState().auth.isAuthenticated
  if(!isAuthBefore && isAuthAfter) {
    dispatch(getProfile())
  }
  dispatch(startRefreshCyle())
  setTimeout(() => dispatch(postRefresh()), refreshInterval)
}

export const logout = () => async (dispatch, getState) => {
  const isAuthBefore = getState().auth.isAuthenticated
  try {
    dispatch(logoutStart())
    await postLogout()
    dispatch(logoutSuccess())
    dispatch(stopRefreshCyle())
    const isAuthAfter = getState().auth.isAuthenticated
    if(isAuthBefore && !isAuthAfter) {
      dispatch(clearSessionProfile())
    }
    //recursive call, which occurs while refresh is valid
    setTimeout(() => dispatch(postRefresh(true)), refreshInterval)
  } catch (err) {
    dispatch(logoutFailure(err.message))
  }
}

export const postRefresh = () => async (dispatch, getState) => {
  const isRefreshCycleActive = getState().auth.isRefreshCycleActive
  const isInitiated = getState().auth.isInitiated
  if(isRefreshCycleActive || !isInitiated) {
    const isAuthBefore = getState().auth.isAuthenticated
    try {
      dispatch(refreshStart())
      await postRefreshToken()
      dispatch(refreshSuccess())
      const isAuthAfter = getState().auth.isAuthenticated
      if(!isAuthBefore && isAuthAfter) {
        dispatch(getProfile())
      }
      //recursive call, which occurs while refresh is valid
      if(!isRefreshCycleActive) {
        dispatch(startRefreshCyle())
      }
      setTimeout(() => dispatch(postRefresh(true)), refreshInterval)
    } catch (err) {
      dispatch(refreshFailure(err.message))
      if(isAuthBefore) {
        dispatch(clearSessionProfile())
      }
    }
  }  
}

export const sendPasswordResetEmail = email => async dispatch => {
  try {
    dispatch(emailPasswordResetStart())
    await postPasswordResetEmail(email)
    dispatch(emailPasswordResetSuccess())
  } catch (err) {
    dispatch(emailPasswordResetFailure(err.message))
  }
}

export const resetPassword = (email, token, password, isPage) => async dispatch => {
  try {
    dispatch(resetPasswordStart())
    await postResetPassword(email, token, password)
    if(isPage) {
      Router.push('/sign-in');
    }
    dispatch(resetPasswordSuccess())
  } catch (err) {
    dispatch(resetPasswordFailure(err.message))
  }
}