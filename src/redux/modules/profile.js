import { createSlice } from '@reduxjs/toolkit'
import { fetchCurrentProfile, patchProfile } from '../../fetch/Profile'

const initialState = {
  details: {},
  profileLoading: false,
  profileError: null
}

const start = (state) => {
  state.profileLoading = true
  state.profileError = null
}

const success = (state, action) => {
  state.details = action.payload
  state.profileLoading = false
}

const fail = (state, action) => {
  state.profileError = action.payload
  state.details = {}
  state.profileLoading = false
}

const profile = createSlice({
  name: 'profile',
  initialState,
  reducers: {
    getProfileStart(state) {
      start(state)
    },
    createProfileStart(state) {
      start(state)
    },
    updateProfileStart(state) {
      start(state)
    },
    getProfileSuccess(state, action) {
      success(state, action)
    },
    updateProfileSuccess(state, action) {
      success(state, action)
    },
    getProfileFailure(state, action) {
      console.warn("Could not retrieve profile:  " + action.payload)
      fail(state, action)
    },
    createProfileFailure(state, action) {
      console.warn("Could not create profile:  " + action.payload)
      fail(state, action)
    },
    updateProfileFailure(state, action) {
      console.warn("Could not update profile:  " + action.payload)
      fail(state, action)
    },
    clearSessionProfile(state) {
      state.details = {}
    }
  }
})

export const {
  getProfileStart,
  getProfileSuccess,
  getProfileFailure,
  clearSessionProfile,
  createProfileStart,
  createProfileFailure,
  updateProfileStart,
  updateProfileSuccess,
  updateProfileFailure
} = profile.actions
export default profile.reducer

export const getProfile = () => async dispatch => {
  try {
    dispatch(getProfileStart())
    const response = await fetchCurrentProfile()
    dispatch(getProfileSuccess(response))
  } catch (err) {
    dispatch(getProfileFailure(err.message))
  }
}

export const updateProfile = (payload) => 
  async dispatch => {
  try {
    dispatch(updateProfileStart())
    const response = await patchProfile(payload)
    dispatch(updateProfileSuccess(response))
  } catch (err) {
    dispatch(updateProfileFailure(err.message))
  }
}