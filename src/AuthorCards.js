import React, { useState, useEffect } from 'react';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import { fetchAuthorStories } from './fetch/Story';
import CssBaseline from '@material-ui/core/CssBaseline';
import PublishedCard from './PublishedCard';

const useStyles = makeStyles(theme => ({
  root: {
    //height: '100vh',
  },
  paper: {
    margin: theme.spacing(8, 1, 1, 1),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
}));

const AuthorCards = (props) => {

  const classes = useStyles();

  const [stories, setStories] = useState();

  useEffect(() => {
    if(props.username) {
      fetchAuthorStories(props.username)
        .then(data => {
          if(data) {
            const mappedData = data.map((story, index) =>
              <Grid key={index} item xs={12} sm={6} md={4}>
                <PublishedCard
                  title={story.title}
                  route={story.route}
                  snippet={story.snippet}
                  lastModifiedDate={story.lastModifiedDate}
                  publicationDate={story.publicationDate}
                />
              </Grid>
            );
            setStories(mappedData);
          }
        })
        .catch(error => {
          console.warn("Could not retrieve author:  " + error);
        });
    }
  }, [props.username]);

  return (
    <Grid container justify="center" component="main" className={classes.root}>
      <CssBaseline />
      <Grid item xs={12} sm={10} lg={8}>
        <div className={classes.paper}>
          <Box mb={5}>
            <Typography component="h1" variant="h5">
              {'@' + props.username + ' ' + props.pageName}
            </Typography>
          </Box>
          <Grid container>
            {stories}
          </Grid>
        </div>
      </Grid>
    </Grid>
  );
};

export default AuthorCards;