import React from 'react';
import { connect } from 'react-redux';
import Router from 'next/router';
import { makeStyles } from '@material-ui/core/styles';
import ListSubheader from '@material-ui/core/ListSubheader';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import BookmarksOutlinedIcon from '@material-ui/icons/BookmarksOutlined';
import FavoriteBorderRoundedIcon from '@material-ui/icons/FavoriteBorderRounded';
import LibraryBooksOutlinedIcon from '@material-ui/icons/LibraryBooksOutlined';
import HistoryRoundedIcon from '@material-ui/icons/HistoryRounded';
import ThumbUpOutlinedIcon from '@material-ui/icons/ThumbUpOutlined';
import CreateOutlinedIcon from '@material-ui/icons/CreateOutlined';
import PublicIcon from '@material-ui/icons/Public';
import AddIcon from '@material-ui/icons/Add';
import Divider from '@material-ui/core/Divider';
import { postCreateWriting } from './fetch/Writing';
import { openLogin } from './redux/modules/auth';
import Link from './Link';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  nested: {
    paddingLeft: theme.spacing(4),
  },
}));

const DrawerList = (props) => {
  const classes = useStyles();

  const handleNew = () => {
    props.handleClose();
    if(!props.isAuthenticated) {
      props.openLogin();
    }
    else {
      postCreateWriting()
      .then((res) => {
        if(res.id){
          const urlPrefix = "/write/";
          const asUrl = urlPrefix + res.id;
          const pageUrl = urlPrefix + "[id]";
          Router.push(pageUrl, asUrl);
        }
      })
      .catch(error => {
        console.error("Could not create new writing:  " + error);
      });
    }
  };

  return (
    <React.Fragment>
    <List
      component="nav"
      aria-labelledby="nested-list-subheader"
      subheader={
        <ListSubheader component="div" id="nested-list-subheader">
          Read
        </ListSubheader>
      }
      className={classes.root}
    >
      <ListItem button onClick={props.handleClose}>
        <ListItemIcon>
          <BookmarksOutlinedIcon />
        </ListItemIcon>
        <ListItemText primary="Bookmarks" />
      </ListItem>
      <ListItem button onClick={props.handleClose}>
        <ListItemIcon>
          <FavoriteBorderRoundedIcon />
        </ListItemIcon>
        <ListItemText primary="Favorites" />
      </ListItem>
      <ListItem button onClick={props.handleClose}>
        <ListItemIcon>
          <HistoryRoundedIcon />
        </ListItemIcon>
        <ListItemText primary="History" />
      </ListItem>
      <ListItem button onClick={props.handleClose}>
        <ListItemIcon>
          <LibraryBooksOutlinedIcon />
        </ListItemIcon>
        <ListItemText primary="Library" />
      </ListItem>
      <ListItem button onClick={props.handleClose}>
        <ListItemIcon>
          <ThumbUpOutlinedIcon />
        </ListItemIcon>
        <ListItemText primary="Recommended" />
      </ListItem>
    </List>
    <Divider />
    <List
      component="nav"
      aria-labelledby="nested-list-subheader"
      subheader={
        <ListSubheader component="div" id="nested-list-subheader">
          Write
        </ListSubheader>
      }
      className={classes.root}
    >
      <ListItem button 
        component={Link} 
        onClick={props.handleClose}
        href="/write/drafts"
        color="textPrimary"
        underline="none"
      >
        <ListItemIcon>
          <CreateOutlinedIcon />
        </ListItemIcon>
        <ListItemText primary="Drafts" />
      </ListItem>
      <ListItem button onClick={handleNew}>
        <ListItemIcon>
          <AddIcon />
        </ListItemIcon>
        <ListItemText primary="New" />
      </ListItem>
      <ListItem button 
        component={Link} 
        onClick={props.handleClose}
        href="/write/published"
        color="textPrimary"
        underline="none"
      >
        <ListItemIcon>
          <PublicIcon />
        </ListItemIcon>
        <ListItemText primary="Published" />
      </ListItem>
    </List>
    </React.Fragment>
    
  );
}

const mapStateToProps = state => {
  const { auth } = state
  return {
    isAuthenticated: auth.isAuthenticated
  }
};

const mapDispatchToProps = { openLogin };

export default connect(mapStateToProps, mapDispatchToProps)(DrawerList);