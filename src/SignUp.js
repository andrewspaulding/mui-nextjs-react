import React, { useState, useRef } from 'react';
import { connect } from 'react-redux';
import Router from 'next/router';
import ReCAPTCHA from 'react-google-recaptcha';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import InputAdornment from '@material-ui/core/InputAdornment';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormHelperText from "@material-ui/core/FormHelperText";
import Checkbox from '@material-ui/core/Checkbox';
import Link from './Link';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import MessageDialog from './MessageDialog';
import { postCreateProfile } from '../src/fetch/Profile';
import { createProfileStart, createProfileFailure } from './redux/modules/profile';
import { handleLoginSuccess, showSignIn } from './redux/modules/auth';

const useStyles = makeStyles(theme => ({
  paperPage: {
    margin: theme.spacing(8, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  paperOverlay: {
    margin: theme.spacing(1, 1),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const SignUp = props => {
  const classes = useStyles();

  const [username, setUsername] = useState('');
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [aboutDescription, setAboutDescription] = useState('');
  const [captchaResponse, setCaptchaResponse] = useState('');

  const [triggerError, setTriggerError] = useState(false);
  const [errorTitle, setErrorTitle] = useState('');
  const [errorMessage, setErrorMessage] = useState('');

  const recaptchaRef = useRef(null);

  const resetTrigger = () => {
    setTriggerError(false);
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    setErrorMessage('');

    props.createProfileStart();
    postCreateProfile(username, name, email, password, aboutDescription, captchaResponse)
      .then(() => {
        console.info("Profile create successful");
        props.handleLoginSuccess();
        if(props.isPage) {
          Router.push(props.authSuccessRedirectPage);
        }
      })
      .catch(error => {
        props.createProfileFailure(error.message);
        console.warn("Profile Create failed: " + error.message);
        setErrorTitle("Error with sign up");
        //setting delay here, so that user feedback is more visible
        setTimeout(() => {
          setErrorMessage(error.message);
        }, 150);
        setTriggerError(true);
      })
      .finally(() => {
        recaptchaRef.current.reset();
      });
  }

  const handleShowSignIn = (event) => {
    event.preventDefault();
    props.showSignIn();
  }


  return (
    <Grid container justify="center" component="main" className={classes.root}>
      <Grid item xs={12} sm={8} md={props.isPage ? 5 : 8}>
        <div className={props.isPage ? classes.paperPage : classes.paperOverlay}>
          {props.isPage ?
            <React.Fragment>
              <Avatar className={classes.avatar}>
                <LockOutlinedIcon />
              </Avatar>
              <Typography component="h1" variant="h5">
                Sign up
              </Typography>
            </React.Fragment>
            : null
          }
          <form className={classes.form} onSubmit={handleSubmit.bind(this)}>
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <TextField
                  name="username"
                  variant="outlined"
                  required
                  fullWidth
                  id="username"
                  label="Username"
                  autoFocus
                  value={username}
                  onChange={(event) => setUsername(event.target.value)}
                  InputProps={{
                    startAdornment: <InputAdornment position="start">@</InputAdornment>
                  }}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  name="name"
                  variant="outlined"
                  required
                  fullWidth
                  id="name"
                  label="Full Name"
                  value={name}
                  onChange={(event) => setName(event.target.value)}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  required
                  fullWidth
                  id="email"
                  label="Email Address"
                  name="email"
                  autoComplete="email"
                  value={email}
                  onChange={(event) => setEmail(event.target.value)}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  required
                  fullWidth
                  name="password"
                  label="Password"
                  type="password"
                  id="password"
                  value={password}
                  onChange={(event) => setPassword(event.target.value)}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  name="aboutDescription"
                  variant="outlined"
                  fullWidth
                  id="aboutDescription"
                  label="About Myself"
                  multiline={true}
                  rows={3}
                  value={aboutDescription}
                  onChange={(event) => setAboutDescription(event.target.value)}
                />
              </Grid>
              <Grid item xs={12}>
                <FormControlLabel
                  control={<Checkbox value="allowExtraEmails" color="primary" />}
                  label="Subscribe to emails"
                />
              </Grid>
              <Grid item xs={12}>
                <ReCAPTCHA
                  sitekey={process.env.recaptchaSiteKey}
                  onChange={(value) => setCaptchaResponse(value)}
                  size="compact"
                  ref={recaptchaRef}
                />
              </Grid>
            </Grid>
            {!props.isPage
              ? <FormHelperText id="component-error-text">
                {errorMessage}
              </FormHelperText>
              : null}
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              Sign Up
              </Button>
            <Grid container justify="flex-end">
              <Grid item>
                {props.isPage
                  ? <Link href="/sign-in" variant="body2">
                    Already have account? Sign in
                  </Link>
                  : <Link href="#" onClick={handleShowSignIn} variant="body2">
                    Already have account? Sign in
                  </Link>
                }
              </Grid>
            </Grid>
          </form>
          {props.isPage
            ? <MessageDialog
              title={errorTitle}
              message={errorMessage}
              resetTrigger={resetTrigger}
              triggered={triggerError}>
            </MessageDialog>
            : null}
        </div>
      </Grid>
    </Grid>
  );
}

const mapStateToProps = state => {
  const { auth } = state
  return {
    authSuccessRedirectPage: auth.authSuccessRedirectPage
  }
};

const mapDispatchToProps = { 
  createProfileStart, 
  handleLoginSuccess,
  createProfileFailure,
  showSignIn
};

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);