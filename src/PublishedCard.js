import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Link from './Link';

const useStyles = makeStyles({
  root: {
    margin: 10,
    height: "95%",
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between"
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginTop: 12,
  },
  cardAction: {
    height: '100%'
  },
  cardContent: {
    height: '100%'
  },

});

const PublishedCard = (props) => {
  const classes = useStyles();

  const formatDate = dateString => {
    const date = new Date(dateString);
    return date.toLocaleString();
  };

  return (
    <Card className={classes.root}>
      <CardActionArea className={classes.cardAction}>
        <CardContent className={classes.cardContent}>
          <Link
            href="/read/[username]/[slug]"
            as={`/read${props.route}`}
            color="textPrimary"
            underline="none">
            <Typography variant="h5" component="h2">
              {props.title}
            </Typography>
            <Typography variant="body2" component="p" className={classes.pos}>
              &quot;...{props.snippet}...&quot;
            </Typography>
            <Typography color="textSecondary" className={classes.pos}>
              Published {formatDate(props.publicationDate)}
            </Typography>
            {props.lastModifiedDate != props.publicationDate ?
              <Typography color="textSecondary">
                Modified {formatDate(props.lastModifiedDate)}
              </Typography> :
              null
            }
          </Link>
        </CardContent>
      </CardActionArea>
    </Card>
  );
}

export default PublishedCard;