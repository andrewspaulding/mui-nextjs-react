import React from 'react';
import { createMuiTheme, ThemeProvider, responsiveFontSizes } from '@material-ui/core/styles';
import { red } from '@material-ui/core/colors';


export function primaryMain() {
  return '#556cd6';
}

const ThemeWrapper = (props) => {
  // const prefersDarkMode = useMediaQuery('(prefers-color-scheme: dark)');

  const theme = React.useMemo(
    () =>
      responsiveFontSizes(createMuiTheme({
        palette: {
          // type: prefersDarkMode ? 'dark' : 'light',
          type: 'dark',
          primary: {
            main: primaryMain(),
          },
          secondary: {
            main: '#19857b',
          },
          error: {
            main: red.A400,
          },
          background: {
            default: '#000',
          }
        }
      })),
    []
  );

  return (
    <ThemeProvider theme={theme}>
      {props.children}
    </ThemeProvider>
  );
};

export default ThemeWrapper;