import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import FormHelperText from "@material-ui/core/FormHelperText";
import Link from './Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import MessageDialog from './MessageDialog';
import CircularProgress from '@material-ui/core/CircularProgress';
import {
  sendPasswordResetEmail,
  resetPassword,
  showSignIn,
  clearError,
  showForgotPassword
} from './redux/modules/auth';

const useStyles = makeStyles(theme => ({
  paperPage: {
    margin: theme.spacing(8, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  paperOverlay: {
    margin: theme.spacing(1, 1),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const ForgotPassword = props => {
  const classes = useStyles();

  const [email, setEmail] = useState('');
  const [triggerError, setTriggerError] = useState(false);
  const [token, setToken] = useState('');
  const [password, setPassword] = useState('');

  const resetTrigger = () => {
    setTriggerError(false);
    props.clearError();
  };

  useEffect(() => {
    setTriggerError(props.error ? true : false);
  }, [props.error]);

  const handleSendEmail = (event) => {
    event.preventDefault();
    props.sendPasswordResetEmail(email);
  }

  const handleSavePassword = (event) => {
    event.preventDefault();
    props.resetPassword(email, token, password, props.isPage);
  }
  
  const handleShowSignIn = (event) => {
    event.preventDefault();
    props.showSignIn();
  }

  const handleShowForgotPassword = (event) => {
    event.preventDefault();
    props.showForgotPassword();
  }

  return (
    <Grid container justify="center" component="main" className={classes.root}>
      <Grid item xs={12} sm={8} md={props.isPage ? 5 : 8}>
        <div className={props.isPage ? classes.paperPage : classes.paperOverlay}>
          {props.isPage ?
            <React.Fragment>
              <Avatar className={classes.avatar}>
                <LockOutlinedIcon />
              </Avatar>
              <Box mb={4}>
              <Typography component="h1" variant="h5">
                Forgot Password
              </Typography>
              </Box>
            </React.Fragment>
            : null
          }
          {!props.displayTokenForm ?
          <form className={classes.form} onSubmit={handleSendEmail.bind(this)}>
            <FormHelperText>
              Your password can be reset by sending a temporary code to your email
            </FormHelperText>

            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="email"
              label="Email"
              name="email"
              autoComplete="email"
              autoFocus
              value={email}
              onChange={(event) => setEmail(event.target.value)}
            />
            {!props.isPage
              ? <FormHelperText id="component-error-text">
                {props.error}
              </FormHelperText>
              : null}
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              disabled={props.loading}
            >
              {!props.loading
                ? "Send Email"
                : <CircularProgress color="inherit" size={24}/>
              }
            </Button>
            <Grid container justify="flex-end">
              <Grid item>
              {props.isPage
                  ? <Link href="/sign-in" variant="body2">
                    Remember password? Sign In
                  </Link>
                  : <Link href="#" onClick={handleShowSignIn} variant="body2">
                    Remember password? Sign In
                  </Link>
                }
              </Grid>
            </Grid>
          </form>
          : null }
          {props.displayTokenForm ?
          <form className={classes.form} onSubmit={handleSavePassword.bind(this)}>
            <FormHelperText>
              {"Please check " + email + " for the temporary code.  It will expire in 30 minutes."}
            </FormHelperText>
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="token"
              label="Temporary Code"
              name="token"
              autoFocus
              value={token}
              onChange={(event) => setToken(event.target.value)}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              label="New Password"
              type="password"
              id="password"
              value={password}
              onChange={(event) => setPassword(event.target.value)}
            />
            {!props.isPage
              ? <FormHelperText id="component-error-text">
                {props.error}
              </FormHelperText>
              : null}
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              Save Password
            </Button>
            <Grid container>
            <Grid item xs>
              <Link href="#" onClick={handleShowForgotPassword} variant="body2">
                Resend Email
              </Link>
              </Grid>
              <Grid item>
              {props.isPage
                  ? <Link href="/sign-in" variant="body2">
                    Remember password? Sign In
                  </Link>
                  : <Link href="#" onClick={handleShowSignIn} variant="body2">
                    Remember password? Sign In
                  </Link>
                }
              </Grid>
            </Grid>
          </form>
          : null }
          {props.isPage
            ? <MessageDialog
              title="Password Reset Error"
              message={props.error}
              resetTrigger={resetTrigger}
              triggered={triggerError}>
            </MessageDialog>
            : null}
        </div>
      </Grid>
    </Grid>
  );
}

const mapStateToProps = state => {
  const { auth } = state
  return {
    loading: auth.authLoading,
    displayTokenForm: auth.displayPasswordResetTokenForm,
    error: auth.authError,
    displayForm: auth.authDisplayForm
  }
};

const mapDispatchToProps = {
  sendPasswordResetEmail,
  resetPassword,
  showForgotPassword,
  showSignIn,
  clearError
};

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPassword);