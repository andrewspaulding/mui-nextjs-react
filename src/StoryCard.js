import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import { green, red } from "@material-ui/core/colors";
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';
import FavoriteIcon from '@material-ui/icons/Favorite';
import LibraryAddOutlined from "@material-ui/icons/LibraryAddOutlined";
import LibraryAddCheckOutlined from "@material-ui/icons/LibraryAddCheckOutlined";
import Typography from '@material-ui/core/Typography';
import Link from './Link';
import SecureToggle from './SecureToggle';

const useStyles = makeStyles({
  root: {
    margin: 10,
    height: "95%",
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between"
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginTop: 12,
  },
  icon: {
    marginLeft: 'auto'
  },
  iconTransition: {
    position: "absolute",
  }
});

const StoryCard = (props) => {
  const classes = useStyles();

  const addIcon = <LibraryAddOutlined color="action" className={classes.iconTransition} />;
  const addedIcon = <LibraryAddCheckOutlined style={{ color: green[500] }} className={classes.iconTransition}/>
  const emptyHeartIcon = <FavoriteBorderIcon color="action" className={classes.iconTransition} />;
  const fullHeartIcon = <FavoriteIcon style={{ color: red[500] }} className={classes.iconTransition}/>

  return (
    <Card className={classes.root}>
      <CardContent>
        <CardActionArea>
          <Link
            href="/read/[username]/[slug]"
            as={`/read${props.route}`}
            color="textPrimary"
            underline="none">
            <Typography variant="h5" component="h2">
              {props.title}
            </Typography>
          </Link>
        </CardActionArea>
        {props.authors && props.authors.length > 0
          ? props.authors.map((username, index) => 
            <CardActionArea key={index}>
              <Link
                href="/read/[username]"
                as={`/read/${username}`}
                color="textSecondary"
                underline="none">
                <Typography key={index} color="textSecondary">
                  @{username}
                </Typography>
              </Link>
            </CardActionArea>)
          : null
        }
        <CardActionArea>
          <Link
            href="/read/[username]/[slug]"
            as={`/read${props.route}`}
            color="textPrimary"
            underline="none">
              <Typography variant="body2" component="p" className={classes.pos}>
                &quot;...{props.snippet}...&quot;
              </Typography>
          </Link>
        </CardActionArea>
      </CardContent>
      <CardActions disableSpacing>
        <SecureToggle
          active={true}
          endpoint="/asdf/library/"
          slug={props.slug}
          inactiveIcon={addIcon}
          inactiveLabel="add to library"
          activeIcon={addedIcon}
          activeLabel="remove from library"
        />
        <SecureToggle
          active={false}
          endpoint="/asdf/favorites/"
          slug={props.slug}
          inactiveIcon={emptyHeartIcon}
          inactiveLabel="add to favorites"
          activeIcon={fullHeartIcon}
          activeLabel="remove from favorites"
        />
      </CardActions>
    </Card>
  );
}

export default StoryCard;