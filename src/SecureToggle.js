import React from "react";
import { connect } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import Zoom from "@material-ui/core/Zoom";
import IconButton from '@material-ui/core/IconButton';
import { openLogin } from './redux/modules/auth';

const useStyles = makeStyles({
  icon: {
    // marginLeft: 'auto'
    width: "48px",
    height: "48px"
  }
});


const SecureToggle = props => {
  const classes = useStyles();

  const [checked, setChecked] = React.useState(props.active);

  const handleChange = () => {

    if(!props.isAuthenticated) {
      props.openLogin();
    }
    else {
      console.log("doing this: " + props.endpoint + props.slug);
      setChecked(prev => !prev);
    }
  };

  return (
    <IconButton
      className={classes.icon}
      aria-label={!checked ? props.inactiveLabel : props.activeLabel}
      onClick={handleChange}>
      <Zoom in={!checked} timeout={{ enter: 200, exit: 700 }}>
        {props.inactiveIcon}
      </Zoom>
      <Zoom in={checked} timeout={{ enter: 200, exit: 700 }}>
        {props.activeIcon}
      </Zoom>
    </IconButton>
  );
};

const mapStateToProps = state => {
  const { auth } = state
  return {
    isAuthenticated: auth.isAuthenticated
  }
};

const mapDispatchToProps = { openLogin };

export default connect(mapStateToProps, mapDispatchToProps)(SecureToggle);