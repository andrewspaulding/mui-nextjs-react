import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import DrawerList from './DrawerList';

const useStyles = makeStyles({
  list: {
    width: 250,
  },
  fullList: {
    width: 'auto',
  },
});

const Drawer = props => {
  const classes = useStyles();
  
// const iOS = process.browser && /iPad|iPhone|iPod/.test(navigator.userAgent);

  const list = (anchor) => (
    <div
      className={clsx(classes.list, {
        [classes.fullList]: anchor === 'top' || anchor === 'bottom',
      })}
      role="presentation"
      onKeyDown={props.handleClose}
    >
      <DrawerList handleClose={props.handleClose} ></DrawerList>
    </div>
  );

  return (
    <SwipeableDrawer
      anchor='left'
      open={props.isOpen}
      onClose={props.handleClose}
      onOpen={props.handleOpen}
      // disableBackdropTransition={!iOS} disableDiscovery={iOS} 
    >
      {list('left')}
    </SwipeableDrawer>
  );
};

export default Drawer;