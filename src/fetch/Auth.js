import { checkStatus, parseJSON } from './FetchCommon';

export const postLogin = (email, password) => {

  let credentials = {
      email: email,
      password
  };

  return (fetch('/auth/login', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(credentials),
  })
    .then(parseJSON)
    .then(checkStatus));
}

export const postLogout = () => {
  return (fetch('/auth/logout', {
    method: 'POST',
  })
    .then(parseJSON)
    .then(checkStatus));
}

export const postRefreshToken = () => {
  return (fetch('/auth/refresh', {
    method: 'POST',
  })
    .then(parseJSON)
    .then(checkStatus));
}

export const postPasswordResetEmail = email => {

  const body = { email };

  return (fetch('/auth/sendPasswordResetEmail', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(body),
  })
    .then(parseJSON)
    .then(checkStatus));
}

export const postResetPassword = (email, token, password) => {

  const body = { 
    email,
    token,
    password
  };

  return (fetch('/auth/resetPassword', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(body),
  })
    .then(parseJSON)
    .then(checkStatus));
}