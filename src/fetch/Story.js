import { checkStatus, parseJSON } from './FetchCommon';

export const fetchReadStory = (username, slug) => {
  return (fetch('/stories/titlebody/' + username + "/" + slug)
    .then(parseJSON)
    .then(checkStatus));
}

export const fetchAuthorStories = (username) => {
  return (fetch('/stories/author/' + username)
    .then(parseJSON)
    .then(checkStatus));
}

export const fetchStoryCards = (offset, limit) => {
  return (fetch('/stories/cards?offset=' + offset + '&limit=' + limit)
    .then(parseJSON)
    .then(checkStatus));
}