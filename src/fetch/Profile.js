import { checkStatus, parseJSON } from './FetchCommon';

export const fetchCurrentProfile = () => {
  return (fetch('/profiles/current')
    .then(parseJSON)
    .then(checkStatus));
}

export const patchProfile = ({name, email, aboutDescription}) => {
  const body = {
    name,
    email,
    aboutDescription
  };

  return (fetch('/profiles/current', {
    method: 'PATCH',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(body),
  })
    .then(parseJSON)
    .then(checkStatus));
}

export const postCreateProfile = (username, name, email, password, aboutDescription, captchaResponse) => {
  const body = {
    username,
    name,
    email,
    password,
    aboutDescription,
    captchaResponse
  };

  return (fetch('/profiles/create', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(body),
  })
    .then(parseJSON)
    .then(checkStatus));
}