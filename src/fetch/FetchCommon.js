export const parseJSON = response => response.json();

export const checkStatus = response => {
  if (response.status >= 400) {
    const error = new Error();
    error.name = response.error;
    error.message = response.message;
    throw error;
  }
  return response;
}