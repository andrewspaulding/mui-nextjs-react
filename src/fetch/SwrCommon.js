export const fetchPostSwr = async (...args) => {
  const res = await fetch(...args, { method: 'POST'});
  return res.json();
}