import { checkStatus, parseJSON } from './FetchCommon';

export const patchPassword = (currentPassword, newPassword) => {
  const body = {
    currentPassword,
    newPassword
  };
  
  return (fetch('/auth/update', {
    method: 'PATCH',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(body),
  })
    .then(parseJSON)
    .then(checkStatus));
}