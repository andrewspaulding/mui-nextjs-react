import { checkStatus, parseJSON } from './FetchCommon';

export const fetchWriting = (id) => {
  return (fetch('/writing/' + id)
    .then(parseJSON)
    .then(checkStatus));
}

export const postCreateWriting = () => {
  return (fetch('/writing/create', {
    method: 'POST'
  })
    .then(parseJSON)
    .then(checkStatus));
}

export const postSaveWriting = (id, change) => {
  return (fetch('/writing/save/body/' + id, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(change),
  })
    .then(parseJSON)
    .then(checkStatus));
}

export const postSaveTitle = (id, title) => {
  const body = {
    title
  };

  return (fetch('/writing/save/title/' + id, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(body),
  })
    .then(parseJSON)
    .then(checkStatus));
}

export const fetchProfileDrafts = () => {
  return (fetch('/writing/drafts')
    .then(parseJSON)
    .then(checkStatus));
}

export const fetchProfilePublished = () => {
  return (fetch('/writing/published')
    .then(parseJSON)
    .then(checkStatus));
}