import React, { useState } from 'react';
import { connect } from 'react-redux';
import Router from 'next/router';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormHelperText from "@material-ui/core/FormHelperText";
import Checkbox from '@material-ui/core/Checkbox';
import Link from './Link';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import MessageDialog from './MessageDialog';
import { postLogin } from './fetch/Auth';
import {
  loginStart,
  handleLoginSuccess,
  loginFailure,
  showSignUp,
  showForgotPassword,
  clearError
} from './redux/modules/auth';

const useStyles = makeStyles(theme => ({
  paperPage: {
    margin: theme.spacing(8, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  paperOverlay: {
    margin: theme.spacing(1, 1),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const SignIn = props => {
  const classes = useStyles();

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [triggerError, setTriggerError] = useState(false);
  const [errorTitle, setErrorTitle] = useState('');
  const [errorMessage, setErrorMessage] = useState('');


  const resetTrigger = () => {
    setTriggerError(false);
    props.clearError();
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    setErrorMessage('');

    props.loginStart();
    postLogin(email, password)
      .then(() => {
        console.info("Login successful");
        props.handleLoginSuccess();
        if(props.isPage) {
          Router.push(props.authSuccessRedirectPage);
        }
      })
      .catch(error => {
        props.loginFailure(error.message);
        console.warn("Login failed: " + error.message);
        setErrorTitle("Login failed");
        //setting delay here, so that user feedback is more visible
        setTimeout(() => {
          setErrorMessage(error.message);
        }, 150);
        setTriggerError(true);
      });
  }

  const handleShowSignUp = (event) => {
    event.preventDefault();
    props.showSignUp();
  }

  const handleShowForgotPassword = (event) => {
    event.preventDefault();
    props.showForgotPassword();
  }

  return (
    <Grid container justify="center" component="main" className={classes.root}>
      <Grid item xs={12} sm={8} md={props.isPage ? 5 : 8}>
        <div className={props.isPage ? classes.paperPage : classes.paperOverlay}>
          {props.isPage ?
            <React.Fragment>
              <Avatar className={classes.avatar}>
                <LockOutlinedIcon />
              </Avatar>
              <Typography component="h1" variant="h5">
                Sign in
              </Typography>
            </React.Fragment>
            : null
          }
          <form className={classes.form} onSubmit={handleSubmit.bind(this)}>
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="email"
              label="Email"
              name="email"
              autoComplete="email"
              autoFocus
              value={email}
              onChange={(event) => setEmail(event.target.value)}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
              value={password}
              onChange={(event) => setPassword(event.target.value)}
            />
            <FormControlLabel
              control={<Checkbox value="remember" color="primary" />}
              label="Remember me"
            />
            {!props.isPage
              ? <FormHelperText id="component-error-text">
                {errorMessage}
              </FormHelperText>
              : null}
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              Sign In
            </Button>
            <Grid container>
              <Grid item xs>
              {props.isPage
                  ? <Link href="/forgot-password" variant="body2">
                    {"Forgot password?"}
                  </Link>
                  : <Link href="#" onClick={handleShowForgotPassword} variant="body2">
                    {"Forgot password?"}
                  </Link>
                }
              </Grid>
              <Grid item>
                {props.isPage
                  ? <Link href="/sign-up" variant="body2">
                    {"Don't have an account? Sign Up"}
                  </Link>
                  : <Link href="#" onClick={handleShowSignUp} variant="body2">
                    {"Don't have an account? Sign Up"}
                  </Link>
                }
              </Grid>
            </Grid>
          </form>
          {props.isPage
            ? <MessageDialog
              title={errorTitle}
              message={errorMessage}
              resetTrigger={resetTrigger}
              triggered={triggerError}>
            </MessageDialog>
            : null}
        </div>
      </Grid>
    </Grid>
  );
}

const mapStateToProps = state => {
  const { auth } = state
  return {
    authSuccessRedirectPage: auth.authSuccessRedirectPage
  }
};

const mapDispatchToProps = {
  loginStart,
  handleLoginSuccess,
  loginFailure,
  showSignUp,
  showForgotPassword,
  clearError
};

export default connect(mapStateToProps, mapDispatchToProps)(SignIn);